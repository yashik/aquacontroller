#include <Arduino.h>
#include "utils/timeout.h"


timeout_on_time timeout(2000);

void setup()  {
	Serial.begin(115200);
}

void loop()
{
	if ( !timeout )
	{
		long elapsed = timeout.elapsed();
		Serial.print("Going to sleep 1 sec, elapsed time=");
		Serial.println(elapsed);
		delay(1000);
	}
	else
	{
		long elapsed = timeout.elapsed();
		Serial.print("Time expired, elapsed time=");
		Serial.println(elapsed);
		timeout.init(); // restart timeout
	}
}

