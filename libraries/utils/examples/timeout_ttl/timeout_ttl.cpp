#include <Arduino.h>
#include "utils/timeout.h"


timeout_on_time timeout(2000);

void setup()  {
	Serial.begin(115200);
}

void loop()
{
	long ttl = timeout.ttl();
	if ( ttl >0 )
	{
		Serial.print("Going to sleep 1 sec, time to live=");
		Serial.println(ttl);
		delay(1000);
	}
	else
	{
		long elapsed = timeout.elapsed();
		Serial.print("Time expired, elapsed time=");
		Serial.print(elapsed);
		Serial.print(" ttl=");
		Serial.println(ttl);
		timeout.init(); // restart timeout
	}
}

