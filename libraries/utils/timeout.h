/*
 * Copyright (C) 2012 Yakov Gerlovin
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the author(s) be held liable for any damages
 * arising from the use of this software.
 */
/** 
 * @file utils/timeout.h
 * @brief [C++ only] Definition for timouts objects
 * @defgroup group__utils Utilities
 * @example examples/timeout_on_time/timeout_on_time.cpp
 * @example examples/timeout_ttl/timeout_ttl.cpp
 */
#ifndef __TIMEOUT_H__
#define __TIMEOUT_H__

#include <Arduino.h>

/**
 * @brief Represents time-based expiration object.
 * The time is expired if elapsed time since last call to @ref init is greater,
*  than the expiration interval passed to constructor
 * @ingroup group__utils
 */
struct timeout_on_time
{
	/// Time type. Should be equal to return type of millis()
	typedef unsigned long time_type;
	
	/// Time difference type. 
	typedef long difference_type;
	
	/// Timestamp of operation start in milli seconds
	time_type start;
	/// Expiration interval in milliseconds
	time_type max_ms;
	
	/**
	* @brief Constructs time-based expiration object with specified expiration interval.
	* Calls @ref init
	* @param ms - maximal time in milli seconds for operation to complete
	*/
	timeout_on_time(time_type ms) : max_ms(ms) { init(); }
	
	/// Initialize operation start. May be called several times
	void init() { start = millis(); }
	
	/// Returns the elapsed time since the last call to @ref init. Note, constructor calls init
	time_type elapsed() const { return millis()-start; }
	
	/// Returns time to live
	difference_type ttl() const { return max_ms - elapsed(); }
	
	/// Returns true if expired
	operator bool() const { return ttl() < 0; }
};

#endif
