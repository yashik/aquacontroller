/*
 * version 1.0, June 3d, 2012
 *
 * Copyright (C) 2012 Yakov Gerlovin
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the author(s) be held liable for any damages
 * arising from the use of this software.
 */


#ifndef __LEDS_LIB_H__
#define __LEDS_LIB_H__

#include "rgb_led.h"
#include "seven_segments.h"

/** 
 * @file leds/leds_lib.h
 * @brief [C++ only] Interface for LEDs library for working with RGB Leds ( @ref Leds::RGBLed "RGBLed", @c RGBLedCA, @c RGBLedCC)  and 7-segments displays (@ref Leds::SevenSegmentDigit "SevenSegmentDigit")
 * @example  leds/examples/RGBLedCA/RGBLedCA.ino
 *
/** 
@mainpage
@section led_lib LED library
@subsection rgb_led RGB Leds 
(Defined in @ref rgb_led.h)
The library provides tools for working with RGB LEDs.<br>
<UL>
<li> @c RGBLedCA - Common Anode RGB LED
<li> @c RGBLedCC - Common Cathode RGB LED
</UL>
@subsection seven_segment 7-Segments LED display
(Defined in @ref seven_segments.h)
Classes for 7 segments displays:
<UL>
<li> @c SevenSegmentDigitCA - Common Anode 7 segments digit
<li> @c SevenSegmentDigitCC - Common Cathode 7 segments digit
</UL>
@subsection led_traits LEDs properties
(Defined in @ref led_traits.h)
In common anode LEDs the common pin is connected to '+' (HIGH), so LED component is turned on when the controlling pin goes LOW.
Common cathode LEDs have the common pin which is connected to GND, so LED component is turned on when the controlling pin goes HIGH.
LED traits incapsulates this information, providing flat interface for both types
<UL>
<LI> @c CommonAnodeLedTraits   - defines @c LED_ON is @c LOW and @c LED_OFF as @c HIGH
<LI> @c CommonCathodeLedTraits - defines @c LED_ON is @c HIGH and @c LED_OFF as @c LOW
</UL>
*/
#endif
