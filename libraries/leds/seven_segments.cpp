/*
 * Copyright (C) 2012 Yakov Gerlovin

 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the author(s) be held liable for any damages
 * arising from the use of this software.
 */
#include "seven_segments.h"

namespace Leds
{
const uint8_t SevenSegmentsCommon::digits_masks[] = {
	SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F, // 0
	SEG_B | SEG_C,                                 // 1
	SEG_A | SEG_B | SEG_G | SEG_E | SEG_D,         // 2
	SEG_A | SEG_B | SEG_G | SEG_C | SEG_D,         // 3
	SEG_F | SEG_B | SEG_G | SEG_C,                 // 4
	SEG_A | SEG_F | SEG_G | SEG_C | SEG_D,         // 5
	SEG_A | SEG_F | SEG_G | SEG_C | SEG_D | SEG_E, // 6
	SEG_A | SEG_B | SEG_C,                         // 7
	SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G, //8 
	SEG_A | SEG_B | SEG_C | SEG_D | SEG_F | SEG_G  //9
};

#if 0
SevenSegmentDigit<CommonAnodeLedTraits,SevenSegmentPinsRandom> d1;
SevenSegmentDigit<CommonCathodeLedTraits,SevenSegmentPinsRandom> d2;
SevenSegmentDigit<CommonAnodeLedTraits,SevenSegmentPinsSequencial> d3;
SevenSegmentDigit<CommonCathodeLedTraits,SevenSegmentPinsSequencial> d4;
#endif

}

