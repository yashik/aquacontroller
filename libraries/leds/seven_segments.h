/*
 * Copyright (C) 2012 Yakov Gerlovin
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the author(s) be held liable for any damages
 * arising from the use of this software.
 */

/** 
 * @file leds/seven_segments.h - interface for 7-Segments LED display
 * @brief [C++ only] Definition of the @c SevenSegmentDigit ( @ref group__leds )
 * @example leds/examples/SevenSegmentsCA/SevenSegmentsCA.ino
 * @example leds/examples/SevenSegmentsCC/SevenSegmentsCC.ino
 * @example leds/examples/SevenSegmentsSeqCA/SevenSegmentsSeqCA.ino
 *
 */
#ifndef __SEVEN_SEGMENTS_H__
#define __SEVEN_SEGMENTS_H__

#include <Arduino.h>
#include "led_traits.h"
namespace Leds
{

/**
* @brief Provides 'gfedcba' encoding for 7 segments display
*
@code
      a
      _ 
  f |   | b
      _       <- g
  e |   | c
      _
	  d
@endcode
* @ingroup group__leds
*/
struct SevenSegmentsCommon
{
	/// Segment values for 'gfedcba' encoding
	enum { SEG_A = 1, SEG_B = 2, SEG_C = 4, SEG_D = 8, SEG_E = 16, SEG_F=32, SEG_G = 64 };
	/// Contains encoding for digits 0-9
	static const uint8_t digits_masks[];
};

/**
 * @brief Provides mapping from segment index to the pin number, 
 * when segments are assigned to non-sequential, random pins
 * @note Requires 7 bytes of memory, one byte for each pin number.
 * When possible, sequential pins assignment should be used with @c SevenSegmentPinsSequencial class to save memory
 * @ingroup group__leds
 */
struct SevenSegmentPinsRandom
{
	/// Array of 7-segment pin numers
	uint8_t pins[7];

	/// Default constructor
	SevenSegmentPinsRandom() {}
	/// Constructor that receives array of 7 pin numbers. The first element (with index 0) correspondes to 'A' segment
	SevenSegmentPinsRandom(uint8_t pins_[7])
	{
		memcpy(pins,pins_,sizeof(pins));
	}
	///Returns pin number for given segment index. Index is zero-based, i.e. 0 correspondes to 'A' segment, 1 to 'B', etc
	uint8_t operator[](uint8_t index) const { return pins[index]; }
};

/**
 * @brief Provides mapping from segment index to the pin number, 
 * when segments are assigned to sequential pins. 
 * 
 * This class contains only the first pin number that correspondes to 'A' segment.
 * The pin number for 'B' segment is @c first_pin + 1, for 'C' segment is @ first_pin +2, etc
 * @ingroup group__leds
 */
struct SevenSegmentPinsSequencial
{
	/// The pin number of 'A' segment
	uint8_t first_pin; 

	/// Constructor that optionally recieves first pin number
	SevenSegmentPinsSequencial(uint8_t first_pin_ = 0) : first_pin(first_pin_) {}
	///Returns pin number for given segment index. Index is zero-based, i.e. 0 correspondes to 'A' segment, 1 to 'B', etc
	uint8_t operator[](uint8_t index) const { return first_pin+index; }
};

/**
 * @brief Represents a single digit of 7-segment display 
 * @param LedTraits - LED type, either @c CommonAnodeLedTraits (default) or @c CommonCathodeLedTraits
 * @param SevenSegmentPins - The type that represents segments-to-pins mapping (e.g. @c SevenSegmentPinsRandom, @c SevenSegmentPinsSequencial)
 * @ingroup group__leds
 */
template < typename LedTraits = CommonAnodeLedTraits, typename  SevenSegmentPins = SevenSegmentPinsRandom >
struct SevenSegmentDigit
{
	enum { LED_ON = LedTraits::LED_ON, LED_OFF = LedTraits::LED_OFF };
	/// Segments-to-pins mapping
	SevenSegmentPins pins;


	/// Sets the pin assignment and sets all pins to @c OUTPUT mode
	void setup(const SevenSegmentPins& pins_)
	{
		pins = pins_;
		for(int i=0; i<7; ++i)
		{
			pinMode(pins[i], OUTPUT);
			digitalWrite(pins[i], LED_OFF);
		}
	}
	/// Shows symbol on 7-segment display
	bool show(uint8_t symbol)
	{
		if ( symbol>='0' && symbol<='9' )
		{
			uint8_t segments_masks = SevenSegmentsCommon::digits_masks[symbol-'0'];

			for ( uint8_t i = 0; i <7; ++i )
			{
				digitalWrite(pins[i], segments_masks & (1<<i) ? LED_ON : LED_OFF );
			}
			return true;
		}
		return false;
	}
};

/**
 * @brief Common Anode 7 segments digit
 * @ingroup group__leds 
 */
typedef SevenSegmentDigit<CommonAnodeLedTraits> SevenSegmentDigitCA;
 
/**
 * @brief Common Cathode 7 segments digit
 * @ingroup group__leds
 */
typedef SevenSegmentDigit<CommonCathodeLedTraits> SevenSegmentDigitCC;

}
#endif

