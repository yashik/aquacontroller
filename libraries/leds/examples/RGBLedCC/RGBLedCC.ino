#include "rgb_led.h"

/* 
 * This example shows how to work with 4 legs Common Cathode RGB led
 * Connect the cathode to GND (ground), the RED leg to pin 40, GREEN leg to 42 and the BLE leg to pin 44 of your arduino.
 * You may change the pin assignment in setup method
 *
 * Connect to serial interface and press r,g,b (or 1,2,3) to light the corresponding color
 */

Leds::RGBLedCC led;

void setup()
{
    Serial.begin(115200);
	led.setup(40, 42, 44);
}
void print_color(int color_mask)
{
	static const char* colors[] = { "RED ", "GREEN ", "BLUE " };
	if ( color_mask == 0 )
		Serial.println("LED is off");
	else
	{
		for(int i=0; i<3; ++i)
			if ( color_mask & (1<<i) )
				Serial.print(colors[i]);
		Serial.println();	
		
	}
}

void loop() 
{
	int color = 0;
	if ( Serial.available() )
	{
		char c = Serial.read();
		switch  (c | 32 )
		{
			case 'r':
			case '1':
				color = Leds::RGBLedCC::RED;
			break;
			case 'g':
			case '2':
				color = Leds::RGBLedCC::GREEN;
			break;
			case 'b':
			case '3':
				color = Leds::RGBLedCC::BLUE;
			break;
			case '4':
				color = Leds::RGBLedCC::RED | Leds::RGBLedCC::GREEN;
			break;
			case '5':
				color = Leds::RGBLedCC::RED | Leds::RGBLedCC::BLUE;
			break;
			case '6':
				color = Leds::RGBLedCC::GREEN | Leds::RGBLedCC::BLUE;
			break;
			case '7':
				color = Leds::RGBLedCC::RED | Leds::RGBLedCC::GREEN | Leds::RGBLedCC::BLUE;
			break;
		}
	}
	led.color( color );
	print_color( led.color() ); 
	delay(400);
}
