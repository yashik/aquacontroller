#include "rgb_led.h"

/* 
 * This example shows how to work with 4 legs Common Anode RGB led
 * Connect the anode to +5V, the RED leg to pin 40, GREEN leg to 42 and the BLE leg to pin 44 of your arduino.
 * You may change the pin assignment in setup method
 */

Leds::RGBLedCA led;

void setup()
{
	led.setup(40, 42, 44);
}

int colors[] = { Leds::RGBLedCA::RED, Leds::RGBLedCA::GREEN, Leds::RGBLedCA::BLUE };

void loop() 
{
	for (int i=0; i<3; ++i)
	{
		for(int j=0; j<=i; ++j)
		{
			led.color( colors[i] ); // Light on
			delay(200);
			led.color( 0 ); // Turn the LED off
			delay(200);
		}
	}
	delay(2000);
}
