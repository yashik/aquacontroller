
#include "seven_segments.h"

/* This example shows how to work with Common Cathode 7 segments display
  
      a
      _ 
  f |   | b
      _       <- g
  e |   | c
      _
	  d

  Connect the cathode to GND(ground) and the segments a-g to pins 30-36 respectively
  You may change pins assignment in 'setup' method
 */

Leds::SevenSegmentDigitCC digit;

void setup()
{
	Serial.begin(115200);
	uint8_t pins[] = { 30,31,32,33,34,35,36 };
	digit.setup(pins);
}

void loop()
{
	if ( Serial.available() )
	{
		uint8_t b = Serial.read();
		if ( b>='0' && b<='9' )
		{
			digit.show(b);
		}
		else
		{
			for(int i=0; i<10; ++i)
			{
				 digit.show(i+'0');
				 delay(300);
			}
		}
		delay(1000);
	}
}
