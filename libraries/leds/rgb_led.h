/*
 * Copyright (C) 2012 Yakov Gerlovin
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the author(s) be held liable for any damages
 * arising from the use of this software.
 */
 
/** 
 * @file leds/rgb_led.h - interface for RGB LEDs
 * @brief [C++ only] Definition of the @c RGBLed template class and its specialization: 
 * @c RGBLedCA (common anode) and @c RGBLedCC (common cathode) ( @ref group__leds )
 */
#ifndef __RGB_LED_H__
#define __RGB_LED_H__

#include <Arduino.h>
#include "led_traits.h"

namespace Leds
{
//! @cond Doxygen_Suppress
// Retrieves the pin number 
#define PIN_NUM(pin) ( (pin) & 0x7F)
// Returns zero for if LED is in logical OFF or 0x80 if LED is in logical ON status
#define PIN_STATUS(pin) ( pin & (1<<7) )
// Sets logical LED status
#define SET_PIN_STATUS(pin, status) \
	if ( status ) pin |= 1<<7; else pin &= 0x7F;
//! @endcond
/**
 * @brief Represents 4 legs RGB led with controlled on-off value for each color.
 * @note This class does not support specifying color intensity and will treat PWM pins as regular pins
 * @ingroup group__leds
 */
template <typename LedTraits = CommonAnodeLedTraits>
class RGBLed
{
	enum { LED_ON = LedTraits::LED_ON, LED_OFF = LedTraits::LED_OFF };
	
	enum Pins
	{
		RED_PIN   = 0,
		GREEN_PIN = 1,
		BLUE_PIN  = 2
	};
public:
	/// Color masks for @c color methods
	enum ColorsMasks
	{ 
		RED   = 1<<RED_PIN,
		GREEN = 1<<GREEN_PIN,
		BLUE  = 1<<BLUE_PIN
	};

private:
	/// 7 LS bits is for pin number and the MSBit for configured value
	uint8_t pins[3];
	
public:
	RGBLed()
	{
		memset(pins,0,sizeof(pins));
	}

	/// Assigns pin numbers to RGBLed and turns off the LED
	void setup(uint8_t red_pin, uint8_t green_pin, uint8_t blue_pin)
	{
		pins[RED_PIN]   = PIN_NUM(red_pin);
		pins[GREEN_PIN] = PIN_NUM(green_pin);
		pins[BLUE_PIN]  = PIN_NUM(blue_pin);
		setup();
	}
	
	/**
	 * @brief Sets LED color, according to given mask
	 * @param mask - contains colors to be turned on
	 * @note Access to HW only if the status has been changed
	 * @ingroup group__leds
	 */
	void color(uint8_t mask)
	{
		for(uint8_t i =0; i<3; ++i)
		{
			uint8_t new_value = ((mask >> i) & 0x1) << 7; // either 0 or 0x80
			if ( new_value != PIN_STATUS(pins[i]) )
			{
				digitalWrite( PIN_NUM(pins[i]), new_value ? LED_ON : LED_OFF );
				SET_PIN_STATUS(pins[i], new_value);
			}
		}
	}
	
	/**
	 * @brief Returns bitmask of LED components that are turned on
	 * @note No access to HW is performed, the function just returns a configured value
	 * @ingroup group__leds
	 */
	uint8_t color() const
	{
		uint8_t value = 0;
		for(uint8_t i =0; i<3; ++i)
		{
			value |= (PIN_STATUS(pins[i])) ? ( 1<<i ) : 0;
		}
		return value;
	}
	
	
private:
	void setup()
	{
		for(uint8_t i = 0; i<3; ++i)
		{
			pinMode( PIN_NUM(pins[i]), OUTPUT);
			digitalWrite(PIN_NUM(pins[i]), LED_OFF);
		}
	}	
};

/**
 * @brief Common Anode RGB LED
 * @ingroup group__leds
 */
typedef RGBLed<CommonAnodeLedTraits> RGBLedCA;

/**
 * @typedef RGBLedCC
 * @brief Common Cathode RGB LED
 * @ingroup group__leds
 */
typedef RGBLed<CommonCathodeLedTraits> RGBLedCC;

}

#endif
