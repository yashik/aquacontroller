/*
 * Copyright (C) 2012 Yakov Gerlovin
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the author(s) be held liable for any damages
 * arising from the use of this software.
 */


/** 
 * @defgroup group__leds LEDs library
 *
 * @file leds/led_traits.h
 * @brief [C++ only] Combined LEDs (e.g. RGB LEDs) and LED displays (e.g. 7-segment) could be in two versions: 
 * Common Anode with all the LED anodes connected together and Common Cathode with all the cathodes connected together.
 * LED traits encapsulates information about a particular LED type ( @ref group__leds )
 */
#ifndef __LED_TRAITS_H__
#define __LED_TRAITS_H__

#include <Arduino.h>

/// LEDs library
namespace Leds
{

/**
* @brief Traits for common anode LED.
* The LED component is ON when its value is lower then on anode
* @ingroup group__leds
*/

struct CommonAnodeLedTraits 
{
	enum { LED_ON = LOW, LED_OFF = HIGH };
};

/**
* @brief Traits for common cathode LED.
* The LED component is ON when its value is higher then on cathode
* @ingroup group__leds
*/
struct CommonCathodeLedTraits 
{
	enum { LED_ON = HIGH, LED_OFF = LOW };
};

}

#endif
