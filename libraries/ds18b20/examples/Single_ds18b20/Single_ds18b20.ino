/* 
 * This example shows how to work with single ds18b20 temperature sensor from Maxim
 * 
 * Connect a signle sensor in regular power mode ( not in �parasite power� mode )
 * You may change the pin assignment in setup method
 */
#include <Arduino.h>
#include "ds18b20.h"

Sensors::ds18b20 sensor;

void setup()
{
    Serial.begin(115200);
	sensor.setup(22);
	
	Sensors::ds18b20::ROM rom;
	if ( sensor.read_rom( (uint8_t*)&rom, sizeof(rom)  ) )
	{
		for (int i=0; i<7; ++i)
		{
			Serial.print( ((uint8_t*)&rom)[i], HEX );
			Serial.print("-");
		}
		Serial.println( ((uint8_t*)&rom)[7], HEX );
	}
	Sensors::ds18b20::Scratchpad scratchpad;
	memset(&scratchpad,0,sizeof(scratchpad));
	if ( sensor.read_command((uint8_t*)&scratchpad,sizeof(scratchpad), Sensors::ds18b20::READ_SCRATCHPAD) )
	{
		int i = 0;
		for (i=0; i< sizeof(scratchpad)-1; ++i)
		{
			Serial.print( ((uint8_t*)&scratchpad)[i], HEX );
			Serial.print("-");
		}
		Serial.println( ((uint8_t*)&scratchpad)[i], HEX );
		Serial.print( F("Resolution: ") );
		Serial.println(scratchpad.resolution());
		Serial.print("Max conversion time: ");
		Serial.println(scratchpad.max_conversion_time());
	}
}


void loop() 
{
	if ( Serial.available() )
	{
		char b = Serial.read();
		switch ( b | 32 )
		{
			case 'c':
			{
				Serial.println("Async CONVERT");
				unsigned long start = millis();
				sensor.send_convert_command();
				uint8_t v = 1;
				do
				{
					Serial.print(".");
					v = sensor.is_operation_in_progress();
				} while ( (0==v) && sensor.isOK() );
				start = millis() -start;
				Serial.print("\r\nElapsed time ");
				Serial.println(start);
			}
			break;
			case 'b':
			{
				Serial.println("Blocking temp reading");
				unsigned long start = millis();
				if ( !sensor.send_convert_command(true) )
				{
					Serial.println("<-FAILED");
					break;
				}
				start = millis() -start;
				Serial.print("   Elapsed time ");
				Serial.println(start);
			}
			case 'r':
			{
				Serial.println("->Reading temperature ");
				uint8_t buf[2];
				unsigned long start = millis();
				if (  sensor.read_command(buf, 2, Sensors::ds18b20::READ_SCRATCHPAD) )
				{
					start = millis() -start;
					Serial.print( Sensors::ds18b20::scratchpad_to_t100(buf)/100.0f);
					Serial.print("   Elapsed time ");
					Serial.println(start);
				}
				else
					Serial.println("<-FAILED");
			}
			break;
			default:
			{		
			Serial.println("Retrieving T...");
				float t = sensor.get_temperature();
			    if ( sensor.isOK() )
				{
					Serial.print("T=");
					Serial.println(t);
				}
				else
					Serial.println("FAILED");
			}
		}
	}
	delay(500);
}

