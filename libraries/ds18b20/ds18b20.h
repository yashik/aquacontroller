/*
 * Copyright (C) 2012 Yakov Gerlovin
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the author(s) be held liable for any damages
 * arising from the use of this software.
 */

/** 
 * @file ds18b20/ds18b20.h
 * @brief [C++ only] Definition of the @c ds18b20 class for working with single DS18B20 sensor on one-wire bus in regular power mode
 * 'parasite power' mode is not supported
 * The advantages of having a single sensor on the bus:
 * - sensors can be easily replaced, no need to discover and maintain sensor's unique 64-bit ROM code (address)
 * - faster communication, as there is no need to send the address over the wire
 * 
 * @defgroup group__sensors Sensors library
 * @example  examples/Single_ds18b20/Single_ds18b20.ino
 *
 */

#ifndef __DS18B20_H__
#define __DS18B20_H__

#include <inttypes.h>


#define PERFORM_POWER_ON_TEST

/// Sensors library
namespace Sensors
{
/**
 * @brief Represents a DS18B20 temperature sensor
 * @ingroup group__sensors
 */
class ds18b20
{	
public:
	/**
	 * @brief ROM commands are common for 1-wire devices.
	 * Those commands are issued after the bus master has detected a presence pulse
	 */
	enum ROMCommands
	{
		SEARCH_ROM   = 0xF0,
		/**
		 * @brief This command can be used only when there is only one slave on the bus
		 * It allows the bus master to read the slave's 64-bit ROM code without using the Search ROM procedure.
		 */
		READ_ROM     = 0x33,
		MATCH_ROM    = 0x55,
		/**
		 * The master can use this command to address all devices on the bus simultaneously without sending out any ROM code information.
		 */
		SKIP_ROM     = 0xCC,
		ALARM_SEARCH = 0xEC
	};

	/**
	 * @brief DS18B20 Function commands
	 */
	enum Commands
	{
		CONVERT_T        = 0x44,
		WRITE_SCRATCHPAD = 0x4E,
		READ_SCRATCHPAD  = 0xBE,
		COPY_SCRATCH_PAD = 0x48,
		RECALL_EEPROM    = 0xB8,
		READ_PWR_SUPPLY  = 0xB4
	};
	
	/**
	 * @brief Values to be validated during power on tests
	 */
	enum TestConstants
	{
		FAMILY_CODE = 0x28,
		POWER_UP_TEMP = 0x0550
	};

	/**
	 * @brief Sensor state masks
	 */
	enum SensorState
	{
		NOT_INITIALIZED = 0,
		DEVICE_DETECTED = 1,
		TEMP_CONVERSION_PERFORMED = 2,
		POWER_ON_OK     = 4,
		DEVICE_FAMILY_MATCH = 8

	};

	/**
	 * Represents the device ROM code
	 */
	struct ROM
	{
		/// Family code of 1-wire bus device
		uint8_t family_code;
		/// Unique serial numberof the device
		uint8_t serial[6];
		/// CRC of the ROM that is calculated from family_code and serial fields
		uint8_t crc;
	};

	/**
	 * @brief Represents EEPROM memory of the sensor
	 */
	struct Eeprom
	{
		/// Upper alarm trigger register (Th) or User byte 1
		uint8_t th;
		/// Lower alarm trigger register (Tl) or User byte 2
		uint8_t tl;
		/// Configuration register
		uint8_t configuration;
		/// Returns the configured resolution of the sensor: 9-12 bits
		inline uint8_t resolution() const { return ((configuration >> 5) & 0x3) + 9; }
		/// Return the maximum temperature conversion time
		inline float max_conversion_time() const { return 93.75 * ( 1 << ((configuration >> 5) & 0x3) ); }
	};

	/**
	 * @brief Represents SRAM memory of the sensor
	 */
	struct Scratchpad
	{
		/// LSB of temperature register
		uint8_t temperature_lsb;
		/// MSB of temperature register
		uint8_t temperature_msb;
		/// Read-write part of scratchpad with nonvolatile EEPROM storage
		Eeprom  mapped;
		/// Reserved, in current revision should be 0xFF
		uint8_t reserved1;
		/// Reserved, unknown value
		uint8_t reserved2;
		/// Reserved, in current revision should be 0x10
		uint8_t reserved3;
		/// CRC code for bytes 0-7 of scratchpad
		uint8_t crc;
		/// Returns the configured resolution of the sensor: 9-12 bits
		inline uint8_t resolution() const { return mapped.resolution(); }
		/// Return the maximum temperature conversion time
		inline float max_conversion_time() const { return mapped.max_conversion_time(); }
	};
private:
	uint8_t m_pin;
	uint8_t m_state;
	
	
public:
	ds18b20() : m_pin(0), m_state(NOT_INITIALIZED) {}

	/// Initializes sensor connected to the given pin
	void setup(uint8_t pin);
	/// Returns true if device is detected on 1-wire bus
	bool is_detected() const { return flag( DEVICE_DETECTED ); }
	/**
	 *  @brief  Return true if expected temperature sensor is detected and it contains non-default value
	 * (i.e. at least one temperature conversion was performed since device restart)
	 */
	bool isOK() const { return flag( DEVICE_DETECTED | TEMP_CONVERSION_PERFORMED | /*POWER_ON_OK |*/ DEVICE_FAMILY_MATCH ); }
	/// Returns true if device state contains all flags in mask
	bool flag(uint8_t mask) const { return mask==(m_state & mask); }
	/// Returns the temperature in degrees C multiplied by 100
	int16_t get_temperature_t100();
	/// Returns the temperature in degrees C
	float get_temperature();
	/// Returns the assigned port number
	uint8_t port() const { return m_pin; }
	
   /* 
	* Advanced API 
	*/

	/**
	 * @brief Sends Convert command to the sensor.
	 * The maxumim conversion time depends on configured resolution and can be queried with
	 * EEPROM's @ref Eeprom::max_conversion_time call
	 */
	bool send_convert_command(bool blocking = false);
	/**
	 * @brief Returns true if the operation is in progress
	 * This method can be called after issuing 'Convert T' [0x44] or 'Recall EEPROM' [0xB8] commands
	 * to find out the status of the operation
	 */
	bool is_operation_in_progress() const;

	/**
	 * @brief Sends read command, then issues 'read' time slots to receive device response
	 * @param dst - the buffer to store response from the device
	 * @param len - the length of the buffer
	 * @param command - command to send to device.
	 * Currently supported: READ_ROM and READ_SCRATCHPAD (READ_PWR_SUPPLY not tested)
	 * @note This method can not be used when there is more than one device on 1-wire bus, because
	 * it issues SKIP_ROM if command is READ_SCRATCHPAD, this method issues SKIP_ROM first
	 */
	bool read_command(uint8_t* dst,uint8_t len, uint8_t command);
	/// Reads up to @c len bytes from ROM
	bool read_rom(uint8_t* rom, uint8_t len);
	/**
	@brief convert scratchpad reading into temperature (degrees*100)
	@param dst should be at least 2 bytes 
	*/
	static int16_t scratchpad_to_t100(const uint8_t* dst);

private:
	bool reset_pulse();
	bool reset();
	bool write_byte(uint8_t value) const;
    bool read_byte(uint8_t& value) const;
	bool power_on_test();
	
};

}

#endif
