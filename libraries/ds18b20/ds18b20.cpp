#include "ds18b20.h"
#include "Arduino.h"

/*
 * According the datasheet max conversion time is for 12bit resolution and should not exceed 750 ms
 * For 9 bit resolution it takes much less
 */
#define MAX_CONVERSION_TIME_MILLISEC  750

namespace Sensors
{

void ds18b20::setup(uint8_t pin)
{
	m_pin = pin;
	m_state = NOT_INITIALIZED;
	digitalWrite(m_pin, LOW);
	pinMode(m_pin, INPUT); 
}

bool ds18b20::reset()
{
#ifdef PERFORM_POWER_ON_TEST
	if ( !flag(TEMP_CONVERSION_PERFORMED) ) // this is first time after power-up, perform tests
		power_on_test();
#endif
	return reset_pulse();
}

bool ds18b20::read_command(uint8_t* dst,uint8_t len, uint8_t command)
{
	if ( !reset_pulse() || len>9 )
		return false;
	
	if ( command != READ_ROM ) // Read ROM command is only valid when there is a single device on one wire
		write_byte(SKIP_ROM);  // so SKIP_ROM should not be sent for READ_ROM command
	write_byte(command);

	for( ; len; --len, ++dst)
	{
		read_byte(*dst);
	}
	return true;
}

float ds18b20::get_temperature()
{
	float temp = get_temperature_t100()/100.0f;
	return temp;
}

int16_t ds18b20::get_temperature_t100()
{
	int16_t  temp = -27400; // below absolute zero

	bool blocking = false;
	if ( !flag(TEMP_CONVERSION_PERFORMED) )
	{
		blocking = true;
	}

	if ( send_convert_command(blocking) )
	{
		uint8_t buf[2];
		if ( read_command(buf, 2, READ_SCRATCHPAD) )
			temp = scratchpad_to_t100(buf);
	}
	return temp;
}

bool ds18b20::send_convert_command(bool blocking)
{
	bool rc = false;
	if ( reset() )
	{
		write_byte(SKIP_ROM);
		write_byte(CONVERT_T);
		uint16_t retries = MAX_CONVERSION_TIME_MILLISEC;
		if ( blocking )
		{
			unsigned long start = millis();
			uint8_t value = 0;
			// wait until the wire is high... 
			while ( retries && !value )
			{
				delayMicroseconds(1000); // wait one milli
				read_byte(value);
				--retries;
			}
			start = millis() -start;
			if ( retries && 0==(m_state & TEMP_CONVERSION_PERFORMED) )
			{
				char buf[100];
				snprintf_P(buf, sizeof(buf),PSTR("ds18b20 on port %d: T conversion performed, elapsed time %d\n"),m_pin,start);
				Serial.println(buf);
			}
		}
		if ( retries )
		{
			if ( blocking )
				m_state |= TEMP_CONVERSION_PERFORMED;
			rc = true;
		}
		else
		{
			// if the max retries has been reached
			m_state = NOT_INITIALIZED;
		}
	}
	return rc;
}

bool ds18b20::is_operation_in_progress() const
{
	uint8_t value = 0;
	read_byte(value);
	return value==0;  // 0 means the conversion is in progress, 1 means the conversion is done
}

bool ds18b20::read_rom(uint8_t* rom, uint8_t len)
{
	return 	(len<=8) && read_command(rom, len,READ_ROM);
}

bool ds18b20::reset_pulse()
{
	digitalWrite(m_pin, LOW);
	pinMode(m_pin, OUTPUT);
	delayMicroseconds(500); // min 480 microsec
	
	digitalWrite(m_pin, HIGH);
	pinMode(m_pin, INPUT);
	delayMicroseconds(80); // DS18B20 waits 15-60 micro,  
	uint8_t bit = digitalRead(m_pin); // DS18B20 sends presence (low) pulse for 60-240 micro
	if ( bit==0 )
	{
		m_state |= DEVICE_DETECTED; // device on wire detected
	}
	else
	{
		m_state = NOT_INITIALIZED;
	}
	delayMicroseconds(420);
	return bit==0;
}

bool ds18b20::write_byte(uint8_t value) const
{
	if ( !is_detected() )
		return false;

	for(byte n=0; n<8; ++n)
	{
		noInterrupts();
		digitalWrite(m_pin,LOW);
		pinMode(m_pin, OUTPUT);	// drive output low

		if (value & 1) {
			// Write 1 time slot - relase 1-Wire within 15 micro sec
			delayMicroseconds(10);
			digitalWrite(m_pin,HIGH);	// drive output high
			interrupts();
			delayMicroseconds(50);
		} else 
		{
			// Write 0 time slot - hold the bus low for at least 60 micro sec
			delayMicroseconds(60);
			digitalWrite(m_pin,HIGH);	// drive output high
			interrupts();
			//delayMicroseconds(5);
		}
		value >>= 1;
		delayMicroseconds(1); // min 1 micro sec recovery time between time slots
	}

	return true;
}

bool ds18b20::read_byte(uint8_t& value) const
{
	if ( !is_detected() )
		return false;
	value = 0;

	for (byte n=0; n<8; ++n)
	{
		noInterrupts();
		pinMode(m_pin, OUTPUT);
		digitalWrite(m_pin, LOW);
		delayMicroseconds(2);
		pinMode(m_pin, INPUT);
		delayMicroseconds(8);
		byte bit = digitalRead(m_pin);
		interrupts();
		delayMicroseconds(50);
		value = (value >> 1) | (bit<<7); // shift d to right and insert b in most sig bit position
	}
	delayMicroseconds(1); // min 1 micro sec recovery time between time slots
	return true;
}

bool ds18b20::power_on_test()
{
	uint16_t buf;
	bool rc = read_command((uint8_t*)&buf,2,READ_SCRATCHPAD);
	if ( rc && (buf==POWER_UP_TEMP) ) // for little endian board
	{
		m_state |= POWER_ON_OK;
	}
	buf = 0;
	rc = read_command((uint8_t*)&buf,1,READ_ROM);
	if ( rc && ((uint8_t)buf==FAMILY_CODE) )
		m_state |= DEVICE_FAMILY_MATCH;
	
	return flag(DEVICE_DETECTED | POWER_ON_OK | DEVICE_FAMILY_MATCH );
}

int16_t ds18b20::scratchpad_to_t100(const uint8_t* buf)
{
	int16_t temp = ((int)buf[1] <<8) + buf[0];
	if ( temp & 0x8000 )
		temp = (temp ^ 0xffff) + 1;
	
	temp = (6*temp) + temp/4;
	return temp;
}


}
