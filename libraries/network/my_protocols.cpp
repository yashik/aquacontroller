/*
 * version 1.0, March 3, 2013
 *
 * Copyright (C) 2013 Yakov Gerlovin
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the author(s) be held liable for any damages
 * arising from the use of this software.
 */
#include "my_protocols.h"


#ifndef MY_NETWORK

ARPProtocol      arp_protocol;
ICMPProtocol     icmp_protocol;
HTTPProtocol     http_protocol;

static Protocol* s_protocols[] =
{
	&ETH::instance(),
	&arp_protocol,
	&IP::instance(),
	&icmp_protocol,
	&http_protocol
};

void MyProtocols::set_mac(const uint8_t* macaddr)
{
	ETH::instance().setup(macaddr, 53);
}

void MyProtocols::set_ip(uint32_t ip)
{
	IP::instance().ip_address( (uint8_t*)&ip);
}

uint8_t   MyProtocols::size() const
{
	return sizeof(s_protocols)/sizeof(s_protocols[0]);
}

Protocol* MyProtocols::protocol(uint8_t index)
{
	return index<size() ? s_protocols[index] : 0;
}

uint16_t MyProtocols::receive(uint8_t* packet, size_type maxlen)
{
	return ProtocolHandler::receive(packet, maxlen);
}

int16_t  MyProtocols::handle(uint8_t* packet, size_type packetlen, size_type maxbuflen)
{
	return ProtocolHandler::handle(packet, packetlen, maxbuflen);
}


#else
#include"ip_arp_udp_tcp.c"

EthernetProtocol eth_protocol;
HTTPProtocol     http_protocol;

uint8_t   MyProtocols::size() const
{
	return 0;
}

Protocol* MyProtocols::protocol(uint8_t index)
{
	return 0;
}

void MyProtocols::set_mac(const uint8_t* macaddr)
{
	eth_protocol.setup(macaddr, 53);
}
void MyProtocols::set_ip(uint32_t ip)
{
	init_ip_arp_udp_tcp(EthernetProtocol::my_mac, (uint8_t*)&ip , 80);
}

uint16_t MyProtocols::receive(uint8_t* packet, size_type maxlen)
{
	uint16_t len = enc28j60PacketReceive(maxlen, packet);
	if ( len == 0 )
	{
		packetloop_icmp_tcp(packet, len);
	}
	return len;
}

int16_t  MyProtocols::handle(uint8_t* packet, size_type packetlen, size_type maxbuflen)
{
   uint16_t processed =  packetloop_icmp_tcp(packet, packetlen);

   if ( processed && (processed < packetlen) )
   {
	   Serial.print("Processed: ");
	   Serial.print(processed);
	   Serial.print("/");
	   Serial.println(packetlen);

	   if (strncmp("GET ",(char *)&(packet[processed]),4)==0 )
	   {
		   packetlen = http_protocol.on_get((char*)packet + processed, maxbuflen);
		   Serial.println("\n==========================================================\n");

		   char buf[1000];
		   snprintf(buf,sizeof(buf),"HTTP RESP: processed=%d http-len=%d\r\n%s\r\n----------------------------\n", processed, packetlen,packet+processed);
		   Serial.println(buf);

		   processed += packetlen;
		   snprintf(buf,sizeof(buf),"\r\n----------------------------\r\nLast 10 bytes: %s\r\n%s\r\n----------------------------\n", packet+processed-10);
		   Serial.println(buf);
		   www_server_reply(packet, processed );
	   }
   }
   return processed;
}



#endif

HTTPProtocol* MyProtocols::get_http() const
{
	return &http_protocol;
}
