#ifndef __SINGLETONE_H__
#define __SINGLETONE_H__

/** 
 * @brief Singletone wrapper for class @c T
 * The instance is static ( i.e. allocated in compile time, initialized/constructed before the first use )
 * For lazy initialization please use @c DynamicSingletone
 */
template <typename T>
class StaticSingletone
{
public:
	typedef T value_type;
	
private:
	StaticSingletone() {}
	StaticSingletone(const StaticSingletone&);
	StaticSingletone& operator=(const StaticSingletone&);
	
public:
	static value_type& instance() { return s_instance; }
private:
	static value_type s_instance;
};

template <typename T>
T StaticSingletone<T>::s_instance;

#endif
