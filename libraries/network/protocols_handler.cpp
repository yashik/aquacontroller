/*
 * version 1.0, March 3, 2013
 *
 * Copyright (C) 2013 Yakov Gerlovin
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the author(s) be held liable for any damages
 * arising from the use of this software.
 */

#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdio.h>
#include <string.h>
#include "protocols_handler.h"

#if ARDUINO>=100
#include <Arduino.h>
#endif

extern "C"
{
#include "enc28j60.h"
}

#define DP_ETH_RECV 	 1
#define DP_ETH_SEND 	 2
#define DP_ARP_RECV 	 4
#define DP_ARP_SEND 	 8
#define DP_IP_RECV  	16
#define DP_IP_SEND  	32
#define DP_ICMP_RECV  	64
#define DP_ICMP_SEND   128
#define DP_TCP_RECV    256
#define DP_TCP_SEND    512
#define DP_HTTP_RECV  1024
#define DP_HTTP_SEND  2048

//#define PRINT_MASK (DP_ETH_SEND | DP_ARP_SEND)
//#define PRINT_MASK (DP_ETH_RECV | DP_ARP_RECV | DP_ARP_SEND | DP_ICMP_RECV | DP_ICMP_SEND | DP_IP_RECV | DP_IP_SEND)
#define PRINT_MASK (DP_TCP_SEND | DP_HTTP_RECV | DP_HTTP_SEND)
#define IS_PRINT_ENABLED(Prot,method) (PRINT_MASK & (DP_ ## Prot ## _ ## method))

uint32_t calc_sum_16bit_words(uint8_t* buf, uint16_t len, uint32_t initial_sum = 0)
{
	uint32_t sum = initial_sum;
	// 1. Calculate sum of 16-bit words
	while ( len > 1 )
	{
		uint16_t v = buf[0];
		sum += (v << 8) + buf[1];
		buf += 2;
		len -= 2;
	}
	// if odd number, pad with zero
	if ( len )
	{
		sum += ((uint16_t)buf[0]) << 8;
	}
	return sum;
}

/**
 * @brief reads ASCII-Z string, stored in program memory
 * @c buf is filled until eos ( '\0' character ) is copied or no space left
 */
uint16_t read_progmem_string(char* buf, uint16_t maxlen, const char *progmem_s/*PROGMEM*/)
{
	int count = 0;
	while ( maxlen && (*buf++ = pgm_read_byte(progmem_s++)) )
	{
		++count;
		--maxlen;
	}
	return count;
}

int16_t ProtocolHandler::handle(uint8_t* packet, size_type packetlen, size_type maxbuflen )
{
	int16_t i = 0;
	int16_t rc = 0;
	uint16_t processed = 0;
	for( ;i < size() && rc>=0 && processed<=packetlen; ++i )
	{
		rc = protocol(i)->handle(packet, packetlen, processed);
	}
	if ( rc == Protocol::SEND_PACKET )
	{
		for( --i; i >=0; --i )
		{
			rc = protocol(i)->send(packet, maxbuflen, processed);
			if ( rc == Protocol::DISCARD_PACKET )
				break;
		}			
	}
	return rc;
}

uint16_t ProtocolHandler::receive(uint8_t* packet, uint16_t maxlen)
{
	return enc28j60PacketReceive(maxlen, packet);
}


/*************************************************************
 * EthernetProtocol
 *************************************************************/
EthernetProtocol::EthernetProtocol()
{
	memset(my_mac,0,sizeof(my_mac));
}

void EthernetProtocol::setup(const uint8_t* macaddr, uint8_t pinSlaveSelect, bool initSPI)
{
	memcpy(my_mac,macaddr, sizeof(my_mac));

	if ( initSPI )
		enc28j60SpiInit();

	/*initialize enc28j60*/
	enc28j60InitWithCs( (uint8_t*)my_mac, pinSlaveSelect );
	enc28j60clkout(2); // change clkout from 6.25MHz to 12.5MHz
	delay(10);

	for( int i=0; i<3; ++i ) {
		// 0x880 is PHLCON LEDB=on, LEDA=on
		// enc28j60PhyWrite(PHLCON,0b0011 1000 1000 00 00);
		enc28j60PhyWrite(PHLCON,0x3880);
		delay(500);

		// 0x990 is PHLCON LEDB=off, LEDA=off
		// enc28j60PhyWrite(PHLCON,0b0011 1001 1001 00 00);
		enc28j60PhyWrite(PHLCON,0x3990);
		delay(500);
	}

	//
	// 0x476 is PHLCON LEDA=links status, LEDB=receive/transmit
	// enc28j60PhyWrite(PHLCON,0b0000 0100 0111 01 10);
	enc28j60PhyWrite(PHLCON,0x3476);
	delay(100);
}

int16_t EthernetProtocol::handle(uint8_t *packet, size_type plen, size_type& processed)
{
	int16_t rc = 0;
	#if IS_PRINT_ENABLED(ETH,RECV)
	print(packet, plen );
	#endif
	if ( plen>= sizeof(Header))
	{
		rc = sizeof(Header);
		processed += rc;
	}
	return rc;
}

int16_t EthernetProtocol::send(uint8_t *packet, size_type maxlen, size_type& packet_len)
{ 
	if ( packet_len >= sizeof(Header) )
	{
		send(packet,packet_len, true);
	}
	// return discard, since the packet either was already sent or is invalid
	return Protocol::DISCARD_PACKET;
}

void EthernetProtocol::set_header(uint8_t *packet,size_type packet_len)
{
	Header* hdr = (Header*) packet;
	memcpy( hdr->dst_mac, hdr->src_mac, sizeof(hdr->dst_mac));
	memcpy( hdr->src_mac, my_mac, sizeof(hdr->src_mac));
}

int16_t EthernetProtocol::send(uint8_t *packet, size_type packet_len, bool fill_header)
{
	if ( fill_header )
	{
		set_header(packet, packet_len);
	}
	#if IS_PRINT_ENABLED(ETH,SEND)
	print(packet, packet_len );
	#endif	
	enc28j60PacketSend(packet_len,packet);
	return packet_len;
}

uint8_t EthernetProtocol::mac2str(char* buf, uint8_t len, uint8_t* macaddr)
{
	return snprintf_P(buf,len,PSTR("%02x-%02x-%02x-%02x-%02x-%02x"),
		macaddr[0],
		macaddr[1],
		macaddr[2],
		macaddr[3],
		macaddr[4],
		macaddr[5]);
}

void EthernetProtocol::print(const uint8_t* packet,uint16_t plen)
{
	if ( plen >= sizeof(Header) )
	{
		Header* hdr = (Header*) packet;
		char buf[200];
		uint8_t l = 0;
		if ( memcmp(ETH::instance().my_mac,hdr->src_mac, sizeof(hdr->src_mac))==0 )
			l = snprintf_P(buf, sizeof(buf), PSTR("ETH SEND"));
		else
			l = snprintf_P(buf, sizeof(buf), PSTR("ETH RECV"));
		l += snprintf_P(buf+l,sizeof(buf)-l, PSTR(" len=%d type=%x src="), plen, hdr->eth_type());
		l += mac2str(buf+l,sizeof(buf)-l, hdr->src_mac);
		l += snprintf_P(buf+l,sizeof(buf)-l, PSTR(" dst="));
		l += mac2str(buf+l,sizeof(buf)-l, hdr->dst_mac);
		Serial.println(buf);
	}
}


/*************************************************************
 * ARPProtocol
 *************************************************************/
//ARPProtocol::ConstHeader ARPProtocol::s_ConstHeader; // TODO initialize with default values

int16_t ARPProtocol::handle(uint8_t *packet, size_type plen, size_type& processed)
{
	Header* hdr = get_header(packet,plen);
	
	if ( hdr )
	{
		if ( hdr->opcode()!=ARP_REQUEST || !IP::instance().is_my_ip(hdr->target_ip) )
		{
			return DISCARD_PACKET;
		}
		#if IS_PRINT_ENABLED(ARP,RECV)
		print(packet);
		#endif
		processed += sizeof(Header);
		return Protocol::SEND_PACKET;
	}
	return 0;
}

int16_t ARPProtocol::send(uint8_t *packet, size_type maxlen, size_type& packet_len)
{ 
	//
	Header* hdr = get_header(packet, packet_len);
	if ( hdr )
	{
		hdr->ch.oper_l = ARP_RESPONSE;
		// ARP Header
		memcpy(hdr->target_mac,hdr->sender_mac,sizeof(hdr->target_mac));
		memcpy(hdr->sender_mac, ETH::instance().my_mac,sizeof(hdr->sender_mac));
		
		memcpy(hdr->target_ip, hdr->sender_ip, sizeof(hdr->target_ip));
		memcpy(hdr->sender_ip, IP::instance().ip_address(),sizeof(hdr->target_ip));
		
		#if IS_PRINT_ENABLED(ARP,SEND)
		print(packet);
		#endif
		return sizeof(Header) + sizeof(EthernetProtocol::Header); 
	}
	return 0;
}

ARPProtocol::Header* ARPProtocol::get_header(const uint8_t *packet, size_type plen)
{
	EthernetProtocol::Header* eth_hdr = (EthernetProtocol::Header*) packet;
	if ( (plen < sizeof(EthernetProtocol::Header)+ sizeof(Header)) || (eth_hdr->eth_type() != ETH_TYPE) )
		return 0;

	Header* hdr = (Header*)(packet+sizeof(EthernetProtocol::Header));
	if ( hdr->protocol()==PROT_IP  )
		return hdr;
	return 0;
}

void ARPProtocol::print(const uint8_t* packet)
{
	Header* hdr = get_header(packet,60);
	if ( hdr )
	{
		char buf[200];
		uint8_t l = snprintf_P(buf,sizeof(buf),PSTR("ARP %s hw_type=%d protocol=0x%04x hw-len=%d prot-len=%d src-mac="), hdr->opcode_str(), hdr->hw_type(), hdr->protocol(), hdr->ch.hlen, hdr->ch.plen);
		l += EthernetProtocol::mac2str(buf+l,sizeof(buf)-l, hdr->sender_mac);
		l += snprintf_P(buf+l,sizeof(buf)-l,PSTR(" dst-mac="));
		l += EthernetProtocol::mac2str(buf+l,sizeof(buf)-l, hdr->target_mac);
		l += snprintf_P(buf+l,sizeof(buf)-l,PSTR(" src-ip="));
		l += IPProtocol::ip2str(buf+l,sizeof(buf)-l, hdr->sender_ip );
		l += snprintf_P(buf+l,sizeof(buf)-l,PSTR(" dst-ip="));
		l += IPProtocol::ip2str(buf+l,sizeof(buf)-l, hdr->target_ip );
		Serial.println(buf);
	}
}

/*************************************************************
 * IPProtocol
 *************************************************************/
int16_t IPProtocol::handle(uint8_t *packet, size_type plen, size_type& processed)
{
	Header* hdr = get_header(packet, plen, processed);
	if ( hdr )
	{
		if ( is_for_me(hdr) )
		{
			#if IS_PRINT_ENABLED(IP,RECV)
			print(hdr);
			#endif
			//hdr->checksum(0); //clear checksum field to be recalculated on send
			processed += hdr->hdr_len();
			return hdr->hdr_len();
		}
		return DISCARD_PACKET;
	}
	return 0;
}

int16_t IPProtocol::send(uint8_t *packet, size_type maxlen, size_type& packet_len)
{
	return send(packet, packet_len, true);
};

bool IPProtocol::is_for_me(Header *hdr)
{
	uint32_t dst_ip = (uint32_t)hdr->dst_ip;
	return (dst_ip==0) || ( dst_ip==0xffffffff ) || is_my_ip(hdr->dst_ip);
}

int16_t IPProtocol::send(uint8_t *packet, size_type packet_len, bool fill_header)
{
	Header* hdr = get_header(packet, packet_len);
	if ( !hdr )
		return 0;
	if ( fill_header )
	{
		uint32_t* src_ip = reinterpret_cast<uint32_t*>(hdr->src_ip);
		const uint32_t* dst_ip = reinterpret_cast<const uint32_t*>(ip_address());
		set_header(hdr, *src_ip, *dst_ip, packet_len, m_identifier++);
	}
	else
	{
		set_header(hdr, packet_len, m_identifier++);
	}
	#if IS_PRINT_ENABLED(IP,SEND)
	print(hdr);
	#endif
	return ETH::instance().send(packet, packet_len, fill_header);
}

IPProtocol::Header* IPProtocol::get_header(const uint8_t *packet, size_type plen, size_type processed)
{
	EthernetProtocol::Header* eth_hdr = (EthernetProtocol::Header*) packet;
	if ( (plen <= sizeof(EthernetProtocol::Header)+ sizeof(Header)) || (eth_hdr->eth_type() != ETH_TYPE) )
		return 0;
	
	// processed should be equal to  sizeof(EthernetProtocol::Header), unless VLAN tagging is used
	Header* hdr = (Header*)(packet+processed);
	return hdr;
}

void IPProtocol::set_header(Header *hdr, size_type packet_len, uint16_t id)
{
	hdr->total_len(packet_len - sizeof(EthernetProtocol::Header));
	hdr->identification(id);
	hdr->checksum(0);
	uint16_t sum = calc_checksum((uint8_t*)hdr,sizeof(Header));
	hdr->checksum(sum);
}

void IPProtocol::set_header(Header *hdr,uint32_t dst_ip, uint32_t src_ip, size_type packet_len, uint16_t id)
{
	uint32_t* p = reinterpret_cast<uint32_t*>(hdr->dst_ip);
	*p = dst_ip;
	p = reinterpret_cast<uint32_t*>(hdr->src_ip);
	*p = src_ip;
	hdr->flags[0] = 0;
	hdr->flags[1] = 0;
	hdr->set_flags(DONT_FRAGMENT);
	set_header(hdr,packet_len,id);
}

uint8_t IPProtocol::ip2str(char* buf, uint8_t len, const uint8_t* ip)
{
	return snprintf_P(buf,len,PSTR("%d.%d.%d.%d"),
		ip[0],
		ip[1],
		ip[2],
		ip[3]);
}

void IPProtocol::print(const Header* hdr)
{
	if ( hdr->protocol == ICMPProtocol::IP_TYPE )
		return;
	char buf[200];
	int l = 0;
	if ( memcmp(IP::instance().ip_address(), hdr->src_ip, sizeof(hdr->src_ip))==0 )
		l = snprintf_P(buf, sizeof(buf), PSTR("IP SEND"));
	else
		l = snprintf_P(buf, sizeof(buf), PSTR("IP RECV"));
	l += snprintf_P(buf+l,sizeof(buf)-l,PSTR(" ver=%d hdr_len=%d total_len=0x%04x id=0x%04x fragment_offset=%d ttl=%u protocol=%d chksum=0x%x%x src_ip="), hdr->version(), hdr->hdr_len(), hdr->total_len(), hdr->identification(), hdr->fragment_offset(), hdr->ttl, hdr->protocol, hdr->chksum[0], hdr->chksum[1]);
	l += IPProtocol::ip2str(buf+l, sizeof(buf)-l, hdr->src_ip );	
	l += snprintf_P(buf+l,sizeof(buf)-l,PSTR(" dst_ip="));
	l += IPProtocol::ip2str(buf+l, sizeof(buf)-l, hdr->dst_ip );	
	Serial.println(buf);
}

uint16_t IPProtocol::calc_checksum(const uint8_t* hdr, uint16_t plen, uint32_t sum)
{
	// 1. Calculate sum of 16-bit words
	sum += calc_sum_16bit_words((uint8_t*)hdr, plen);

	// 2. calculate 16bit sum of calculated sum in prev step
    while (sum>>16)
    {
        sum = (sum & 0xFFFF) + (sum >> 16);
    }

    // 3. return one's complement
    return ~sum;
}


/*************************************************************
 * ICMPProtocol
 *************************************************************/
int16_t ICMPProtocol::handle(uint8_t *packet, size_type plen, size_type& processed)
{
	Header* hdr = get_header(packet,plen);
	if ( hdr )
	{
		#if IS_PRINT_ENABLED(ICMP,RECV)
		print(hdr);
		#endif
		processed = plen;
		if ( hdr->type==ICMP_REQUEST )
			return Protocol::SEND_PACKET;
		// other ICMP messages are discarded
		return Protocol::DISCARD_PACKET;
	}
	return 0;
}

int16_t ICMPProtocol::send(uint8_t *packet, size_type maxlen, size_type& packet_len)
{
	Header* hdr = get_header(packet, packet_len);
	if ( hdr )
	{
		if ( hdr->type==ICMP_REQUEST )
		{ // correct the checksum. 
			if (hdr->hdr_checksum[0] > (0xff-ICMP_REPLAY))
			{
				++(hdr->hdr_checksum[1]);
			}
			hdr->hdr_checksum[0] += 0x08;			
		}
		hdr->type = ICMP_REPLAY;
		#if IS_PRINT_ENABLED(ICMP,SEND)
		print(hdr);
		#endif
		return packet_len;
	}
	return 0;
};

ICMPProtocol::Header* ICMPProtocol::get_header(const uint8_t *packet,uint16_t plen)
{
	IPProtocol::Header* ip_hdr = IPProtocol::get_header(packet,plen);
	if ( ip_hdr && ip_hdr->protocol==IP_TYPE && ((ip_hdr->total_len() + sizeof(EthernetProtocol::Header))==plen) )
	{
		Header* hdr = (Header*)ip_hdr->data();
		return hdr;
	}
	return 0;
}

void ICMPProtocol::print(const Header* hdr)
{
	char buf[200];
	int l = snprintf_P(buf,sizeof(buf),PSTR("ICMP %s type=%d code=%d checksum=0x%04x"), hdr->type_str(), hdr->type, hdr->code, hdr->checksum() );
	if ( (ICMP_REPLAY==hdr->type) || (ICMP_REQUEST==hdr->type) )
	{
		uint16_t id = NET2HOST_PWORD(hdr->quench);
		uint16_t seq = NET2HOST_PWORD(hdr->quench+2);
		l += snprintf_P(buf+l,sizeof(buf)-l,PSTR(" id=%d seq=%d"),id, seq );
	}
	IPProtocol::Header* ip_hdr = (IPProtocol::Header*) ((char*)hdr-sizeof(IPProtocol::Header));
	if ( hdr->type==ICMP_REPLAY )
	{
		l += snprintf_P(buf+l,sizeof(buf)-l,PSTR(" dst_ip="));
	}
	else
	{
		l += snprintf_P(buf+l,sizeof(buf)-l,PSTR(" src_ip="));
	}
	l += IPProtocol::ip2str(buf+l, sizeof(buf)-l, ip_hdr->src_ip );
	l += snprintf_P(buf+l,sizeof(buf)-l,PSTR(" ttl=%d"), ip_hdr->ttl);
	Serial.println(buf);
}


/*************************************************************
 * TCPProtocol
 *************************************************************/
int16_t TCPProtocol::handle(uint8_t *packet, size_type plen, size_type& processed)
{
	Header* hdr = get_header(packet,plen);
	if ( hdr )
	{
		if ( ! is_for_me(hdr) )
			return DISCARD_PACKET;

		IPProtocol::Header* ip_hdr = IPProtocol::get_header(packet,plen);
		processed += hdr->hdr_len();

		int16_t rc = DISCARD_PACKET;

		if ( hdr->flags & FLAG_RST )
			rc = DISCARD_PACKET;
		else if ( (processed < plen) && (processed < ip_hdr->total_len()+sizeof(EthernetProtocol::Header)) )
		{
			//rc = hdr->hdr_len();
			rc = processed;
		}
		else if ( hdr->flags & FLAG_SYN ) // SYN without data
			rc = SEND_PACKET;
		else if ( (hdr->flags==FLAG_ACK) && (hdr->seq_num()!=1) )
			rc = DISCARD_PACKET;
		else if ( (hdr->flags==(FLAG_ACK | FLAG_FIN)) && (hdr->seq_num()!=1) )
			rc = SEND_PACKET;
		else if ( hdr->flags & (FLAG_ACK | FLAG_FIN) ) //
			rc = SEND_PACKET;
		/*
		if ( hdr->flags == (FLAG_ACK | FLAG_FIN) )
		{
			rc = DISCARD_PACKET;
		}
		*/

		packet[plen] = 0;

		#if IS_PRINT_ENABLED(TCP,RECV)
		char buf[200];
		uint16_t payload_offset = sizeof(EthernetProtocol::Header) + ip_hdr->hdr_len() + hdr->hdr_len();
		(void)payload_offset;
		snprintf_P(buf,sizeof(buf),PSTR("TCP handle: plen=%d flags=%d sport=%u processed=%u payload_offset=%d seq=%lx ack=%lx rc=%d iphdrlen=%d tcphdrlen=%d"), plen, hdr->flags, hdr->src_port(), processed, payload_offset, hdr->seq_num(), hdr->ack_num(),rc,ip_hdr->hdr_len(), hdr->hdr_len());
		Serial.println(buf);
		#endif

		return rc;
	}
	return 0;
}

int16_t TCPProtocol::send(uint8_t *packet, size_type maxlen, size_type& packet_len)
{
	IPProtocol::Header* ip_hdr = IPProtocol::get_header(packet,packet_len);
	Header* hdr = (Header*)ip_hdr->data();
	if ( hdr && is_for_me(hdr) )
	{
		// the received TCP payload len is total IP len - IP and TCP headers
		uint16_t rcv_tcp_paylod   = ip_hdr->total_len() - (ip_hdr->hdr_len()+hdr->hdr_len());
		uint16_t send_tcp_payload = packet_len - (sizeof(IPProtocol::Header) + sizeof(EthernetProtocol::Header));
		make_resp(hdr, rcv_tcp_paylod, send_tcp_payload - hdr->hdr_len() );
		hdr->checksum(0);

		uint32_t sum = IPProtocol::calc_checksum(ip_hdr->src_ip,send_tcp_payload+8, IP_TYPE+send_tcp_payload);
	    hdr->checksum(sum);

		#if IS_PRINT_ENABLED(TCP,SEND)
		char buf[200];
		snprintf_P(buf,sizeof(buf),PSTR("TCP send1:   plen=%d flags=%d dport=%u seq=%lx ack=%lx rcv-tcp-len=%d send-tcp-len=%d  sport=%u\r\n"), packet_len ,hdr->flags, hdr->dst_port(), hdr->seq_num(), hdr->ack_num(), rcv_tcp_paylod, send_tcp_payload, hdr->src_port() );
		Serial.println(buf);
		#endif

		return hdr->hdr_len() + sizeof(IPProtocol::Header) + sizeof(EthernetProtocol::Header);

		return packet_len;
	}
	return 0;
}

bool TCPProtocol::is_for_me(Header *hdr)
{
	uint16_t dst_port = hdr->dst_port();
	return dst_port==23 || dst_port==80;
}

TCPProtocol::Header* TCPProtocol::get_header(const uint8_t *packet,uint16_t plen)
{
	IPProtocol::Header* ip_hdr = IPProtocol::get_header(packet,plen);
	uint16_t embedded_len = ip_hdr->total_len() + sizeof(EthernetProtocol::Header);
	if ( ip_hdr && ip_hdr->protocol==IP_TYPE && (embedded_len==plen || plen==60) )
	{
		Header* hdr = (Header*)ip_hdr->data();
		return hdr;
	}
	return 0;
}

void TCPProtocol::make_resp(Header* hdr, uint16_t request_datalen, uint16_t datalen)
{
	set_ports_from_request(hdr);

	uint32_t orig_snum = hdr->seq_num();
	uint32_t orig_anum = hdr->ack_num();
	uint8_t orig_flags = hdr->flags;
	if ( orig_flags & FLAG_SYN )
	{
		hdr->seq_num(millis());
		hdr->ack_num(orig_snum+1);
		hdr->set_flags( FLAG_SYN | FLAG_ACK | FLAG_PSH );
	}
	else
	{
		int flags = FLAG_PSH;
		hdr->seq_num(orig_anum);
		if ( orig_flags & FLAG_ACK )
		{
			flags  |= FLAG_ACK;
			if ( request_datalen == 0 && (orig_flags & FLAG_FIN) )
				++request_datalen;
			hdr->ack_num(orig_snum + request_datalen);
		}
		else
			hdr->ack_num(0);


		if ( datalen )
		{
			//flags |= FLAG_FIN;
		}

		if ( datalen==0 && (orig_flags & FLAG_FIN) )
			flags |= FLAG_FIN;

		else if ( orig_flags & FLAG_RST )
		{
			flags |= FLAG_RST;
		}
		hdr->set_flags(  flags );
	}
	set_options_from_request(hdr);
}

int16_t TCPProtocol::send(uint8_t *packet, size_type packet_len, bool fill_header)
{
	IPProtocol::Header* ip_hdr = IPProtocol::get_header(packet,packet_len);
	Header* hdr = (Header*)ip_hdr->data();
	if ( hdr )
	{
		uint8_t  orig_flags = hdr->flags;
		uint32_t orig_snum = hdr->seq_num();
		uint32_t orig_anum = hdr->ack_num();

		uint32_t seq_num = -1;
		// the received TCP payload len is total IP len - IP and TCP headers
		uint16_t rcv_tcp_paylod = ip_hdr->total_len() - (ip_hdr->hdr_len()+hdr->hdr_len());
		uint16_t send_tcp_payload = packet_len - (sizeof(IPProtocol::Header) + sizeof(EthernetProtocol::Header));
		uint8_t flags = 0;
		if ( fill_header )
		{
			set_ports_from_request(hdr);
			set_options_from_request(hdr);

			seq_num = orig_anum;
			uint32_t ack_num = 0;
			flags = FLAG_PSH;
			if ( rcv_tcp_paylod == 0 && (orig_flags & FLAG_FIN) )
			{
				++rcv_tcp_paylod;
			}

			if (orig_flags & FLAG_SYN)
			{
				ack_num = orig_snum + 1;
				// if SYN flags is set there is no original Ack, so set seq_num to arbitrary value
				seq_num = millis();
				flags |= (FLAG_SYN | FLAG_ACK | FLAG_PSH);
			}
			// set ack
			else if ( orig_flags & FLAG_ACK )
			{
				ack_num = orig_snum + rcv_tcp_paylod;
				flags   |= FLAG_ACK;
			}
			hdr->ack_num(ack_num);
			hdr->set_flags(  flags );
		}
		else
		{
			flags = orig_flags;
			// Remove SYN,RST flags
			flags &= (~FLAG_SYN);
			flags &= (~FLAG_RST);
			hdr->set_flags(  flags );
			// Handle seq num, ignore ack, since it has already been updated
			// instead, advance the seq_num by already sent payload
			seq_num = orig_snum + rcv_tcp_paylod;
		}
		hdr->seq_num(seq_num);

		hdr->checksum(0);
		uint32_t sum = IPProtocol::calc_checksum(ip_hdr->src_ip,send_tcp_payload+8, IP_TYPE+send_tcp_payload);
	    hdr->checksum(sum);

		#if IS_PRINT_ENABLED(TCP,SEND)
		char buf[200];
		snprintf_P(buf,sizeof(buf),PSTR("TCP send:   plen=%d flags=%d dport=%u seq=%lx ack=%lx rcv-tcp-len=%d send-tcp-len=%d  sport=%u orig_flags=%d"), packet_len ,hdr->flags, hdr->dst_port(), hdr->seq_num(), hdr->ack_num(), rcv_tcp_paylod, send_tcp_payload, hdr->src_port(), orig_flags );
		Serial.println(buf);
		#endif
		return IP::instance().send(packet, packet_len, fill_header);
	}
	return 0;
}

void TCPProtocol::set_ports_from_request(Header* hdr)
{
	uint16_t dst_port = hdr->dst_port();
	hdr->dst_port( hdr->src_port() );
	hdr->src_port( dst_port );
}

void TCPProtocol::set_options_from_request(Header* hdr)
{
	hdr->window(max_window);

	// if there are options, fill it
	uint8_t* options = hdr->options();
	if ( options )
	{
		int options_size = hdr->hdr_len( ) - sizeof(Header);
		if ( options_size >=4 ) // Fill maximum segment size
		{
			options[0] = OPT_MSS;
			options[1] = 4;
			options[2] = max_window>>8;
			options[3] = (uint8_t)max_window;
			options += 4;
			options_size-=4;
		}
		if ( options_size >=4 ) // Fill window scale
		{
			*options++ = OPT_NOP; // put NOP
			*(uint16_t*)options     = 0x0303;
			options += 2;
			*options++ = 0; // no scale
			options_size -=4;
		}
		while ( options_size > 0 ) // Fill the rest of options with NOP to preserve the size of TCP header
		{
			*options++ = OPT_NOP;
			--options_size;
		}
	}
}


const char* HTTPProtocol::methods[] = { "GET ", "POST " };

static  PROGMEM const char http_unauthorized_response[] =
		"HTTP/1.1 401 Unauthorized\r\n"
		"Server: Arduino Webserver\r\n"
		"Connection: close\r\n"
		"Content-Type: text/html\r\n\r\n"
		"<h1>401 Unauthorized</h1>";

static  PROGMEM const char http_ok_header[]  =
		"HTTP/1.1 200 OK\r\n"
		"Server: Arduino Webserver\r\n"
		"Connection: close\r\n"
		"Access-Control-Allow-Origin: *\r\n"
		"Pragma: no-cache\r\n";

static PROGMEM const char http_context_text[] = "Content-Type: text/html\r\n\r\n";
static PROGMEM const char http_context_json[]  = "Content-Type: text/plain; charset=UTF-8\r\nContent-Length:      \r\n\r\n";//application/json

int16_t HTTPProtocol::handle(uint8_t *packet, size_type plen, size_type& processed)
{
	int16_t rc = TCPProtocol::handle(packet, plen, processed);

	if ( (rc <= 0 ) || ( processed>=plen ) )
		return rc;

	if ( processed+MIN_BODY_LENGTH < plen )
	{
		rc = DISCARD_PACKET;
		if ( on_get && strncmp((char*)packet+processed, methods[METHOD_GET],4)==0 )
		{
			rc = SEND_PACKET;
		}
		else if ( on_post && strncmp((char*)packet+processed, methods[METHOD_POST],5)==0)
		{
			rc = SEND_PACKET;
		}
	}

	#if IS_PRINT_ENABLED(HTTP,RECV)
	char buf[200];
	packet[plen] = 0;
	int n  = snprintf_P(buf,sizeof(buf),PSTR("HTTP handle plen=%d processed=%d rc=%d\r\n%s\r\n"), plen, processed, rc, packet+processed );
	(void)n;
	Serial.println(buf);
	#endif
	return rc;
}


int16_t HTTPProtocol::send(uint8_t *packet, size_type maxlen, size_type& packet_len)
{
	IPProtocol::Header* ip_hdr = IPProtocol::get_header(packet,packet_len);
	size_type rcv_plen = ip_hdr->total_len() + sizeof(EthernetProtocol::Header);

	if ( rcv_plen >  packet_len )
	{
		// Send ACK for HTTP request
		TCPProtocol::send(packet, packet_len, true);
			
		char* http_hdr = (char* )packet+packet_len;
		
		size_type payload = 0;
		if ( on_post && (http_hdr[0] =='P')  )
		{
			payload += on_post(http_hdr, maxlen-packet_len);
		}
		else if ( http_hdr[0] =='G')
		{
			payload += on_get(http_hdr, maxlen-packet_len);
		}

		#if IS_PRINT_ENABLED(HTTP,SEND)
		{
			char buf[200];
			int n  = snprintf_P(buf,sizeof(buf),PSTR("HTTP RESP in-plen=%d resp-packet-len=%d maxlen=%d\r\n"), rcv_plen, packet_len+payload, maxlen);
			buf[n] = 0;
			Serial.println(buf);
		}
		#endif
		// Send HTTP response
		TCPProtocol::send(packet, packet_len + payload, false);
		
		// Send TCP FIN (close connection)
		TCPProtocol::Header* tcp_hdr = (TCPProtocol::Header*)ip_hdr->data();
		uint8_t flags = tcp_hdr->flags;
		flags |= FLAG_FIN;
		tcp_hdr->set_flags(flags);
		TCPProtocol::send(packet, packet_len, false);
	}
	else
	{
		Serial.println( F("HTTP send: invalid data received") );
		TCPProtocol::send(packet, packet_len, true);
	}
	return DISCARD_PACKET;
}

int16_t HTTPProtocol::fill_unauthorized(char* http_body, size_type maxlen)
{
	return read_progmem_string(http_body,maxlen,http_unauthorized_response);
}

int16_t HTTPProtocol::fill_ok_header(char* http_body, size_type maxlen)
{
	return read_progmem_string(http_body, maxlen, http_ok_header);
}

int16_t HTTPProtocol::fill_context_type(char* http_body, size_type maxlen, ContextType ctype)
{
	switch (ctype)
	{
		case CONTEXT_TEXT:
			return read_progmem_string(http_body, maxlen, http_context_text);
		case CONTEXT_JSON:
			return read_progmem_string(http_body, maxlen, http_context_json);
	}
	return 0;
}
