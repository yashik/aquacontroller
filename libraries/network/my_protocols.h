/*
 * version 1.0, March 3, 2013
 *
 * Copyright (C) 2013 Yakov Gerlovin
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the author(s) be held liable for any damages
 * arising from the use of this software.
 */

#ifndef __MY_PROTOCOLS_H__
#define __MY_PROTOCOLS_H__
#include "protocols_handler.h"

class MyProtocols : protected ProtocolHandler
{

public:
	MyProtocols() : ProtocolHandler() {}
	void set_mac(const uint8_t* macaddr);
	void set_ip(uint32_t ip);

	virtual uint8_t   size() const;
	virtual Protocol* protocol(uint8_t index);

	uint16_t receive(uint8_t* packet, size_type maxlen);
	int16_t  handle(uint8_t* packet, size_type packetlen, size_type maxbuflen);
	HTTPProtocol* get_http() const;
};

#endif
