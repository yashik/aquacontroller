/*
 * version 1.0, March 3, 2013
 *
 * Copyright (C) 2013 Yakov Gerlovin
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the author(s) be held liable for any damages
 * arising from the use of this software.
 */
#ifndef __PROTOCOLS_HANDLER_H__
#define __PROTOCOLS_HANDLER_H__

#include <inttypes.h>
#include <string.h>
#include "singletone.h"

#define MTU 1400

/**
 Converts WORD in network byte order into host byte order
 for little endian
 */
#define NET2HOST_PWORD( p ) ( (((uint16_t)(p)[0])<<8) | ((p)[1]) )

#define NET2HOST_PDWORD( p ) ( (uint32_t((p)[0])<<24) | (uint32_t((p)[1])<<16) | (uint32_t((p)[2])<<8) | ((p)[3]) )
/** 
 * Converts a word (value) in host byte order into network byte order for little endian
 */
#define HOST2NET_PWORD(value,p) ( ((*(uint8_t*)(p))=((value)>>8)) | ((*((uint8_t*)(p)+1))=((value)&0xFF)) )
#define HOST2NET_PDWORD(value,p) { ((p)[3]) = ((uint8_t*)&value)[0]; ((p)[2]) = ((uint8_t*)&value)[1]; ((p)[1]) = ((uint8_t*)&value)[2]; ((p)[0]) = ((uint8_t*)&value)[3]; }

uint16_t read_progmem_string(char* buf, uint16_t maxlen, /*PROGMEM*/ const char *progmem_s);

struct Protocol
{
	typedef uint16_t size_type;

	enum {
		DISCARD_PACKET = -1,
		SEND_PACKET  = -2
	};
	/**
	@brief handles received packet
	@param packet - the packet
	@param plen - packet length
	@param processed - the number of bytes from the packet that were processed by the lower protocol.
	@returns the number of bytes processed by the handler or -1 to notify that packet should be dropped
	*/
	virtual ~Protocol() {}
	virtual int16_t handle(uint8_t *packet, size_type plen, size_type& processed) = 0;
	virtual int16_t send(uint8_t *packet, size_type maxlen, size_type& packet_len) =0;// { return SEND_PACKET; }
};

class ProtocolHandler
{
public:
	typedef Protocol::size_type size_type;
public:
	virtual ~ProtocolHandler() {}
	uint16_t receive(uint8_t* packet, size_type maxlen);
	int16_t  handle(uint8_t* packet, size_type packetlen, size_type maxbuflen);
	virtual uint8_t   size() const = 0;
	virtual Protocol* protocol(uint8_t index) = 0;
};


struct EthernetProtocol : public Protocol
{
	uint8_t my_mac[6];
	// Total size 14b
	struct Header
	{
		uint8_t dst_mac[6];
		uint8_t src_mac[6];
		uint8_t type[2];
		uint16_t eth_type() const { return NET2HOST_PWORD(type); }
	};
	EthernetProtocol();
	void setup(const uint8_t* macaddr, uint8_t pinSlaveSelect, bool initSPI = true);
	virtual int16_t handle(uint8_t *packet, size_type plen, size_type& processed);
	virtual int16_t send(uint8_t *packet, size_type maxlen, size_type& packet_len);
	void set_header(uint8_t *packet,size_type packet_len);
	int16_t send(uint8_t *packet,size_type packet_len, bool fill_header);

	static uint8_t mac2str(char* buf, uint8_t len, uint8_t* macaddr);
	static void print(const uint8_t* packet, uint16_t plen);
};

typedef StaticSingletone<EthernetProtocol> ETH;

struct ARPProtocol : public Protocol
{
	enum { ETH_TYPE = 0x0806 };
	enum ProtocolType 
	{ 
		PROT_IP = 0x800 
	};
	enum OperationType
	{
		ARP_REQUEST = 1,
		ARP_RESPONSE = 2
    } ;	
	
	// size 8b
	struct ConstHeader
	{
		uint8_t hw_type_h;   // hardware type - high byte
		uint8_t hw_type_l;
		uint8_t prot_type_h; // protocol type - high byte
		uint8_t prot_type_l;
		uint8_t hlen;        // hardware address length
		uint8_t plen;        // protocol address length
		uint8_t oper_h ;     // operation - high byte
		uint8_t oper_l;      // operation - high byte
	};
	struct Header
	{
		ConstHeader ch;
		uint8_t sender_mac[6];
		uint8_t sender_ip[4];
		uint8_t target_mac[6];
		uint8_t target_ip[4];
		
		uint16_t hw_type() const  { return NET2HOST_PWORD(&ch.hw_type_h); } //{ return ch.hw_type_h<<8 | ch.hw_type_l; }
		uint16_t opcode() const   { return NET2HOST_PWORD(&ch.oper_h); } //{ return ch.oper_h<<8 | ch.oper_l; }
		uint16_t protocol() const { return NET2HOST_PWORD(&ch.prot_type_h); } // { return ch.prot_type_h <<8 | ch.prot_type_l; }
		const char* opcode_str() const 
		{
			static const char* str[] = { "Unknown", "REQ", "RSP" };
			return opcode() < 3 ? str[opcode()] :str[0];
		}
	};
	
	virtual int16_t handle(uint8_t *packet, size_type plen, size_type& processed);
	virtual int16_t send(uint8_t *packet, size_type maxlen, size_type& packet_len);
	
	static Header* get_header(const uint8_t *packet, size_type plen);
	
	static void print(const uint8_t* packet);
};

class IPProtocol : public Protocol
{
public:
	enum { ETH_TYPE = 0x0800 };
	enum Flags 
	{
		DONT_FRAGMENT = 2,
		MORE_FRAGMENTS = 4
	};
	struct Header
	{
		uint8_t ver_len; // // header-len is the number of 32-bit words in the header
		uint8_t services_cong; // services - 6 bits, congestion - 2 bits
		uint8_t tot_len[2];
		uint8_t id[2];
		uint8_t flags[2];
		uint8_t ttl;
		int8_t  protocol;
		uint8_t chksum[2];
		uint8_t src_ip[4];
		uint8_t dst_ip[4];
		
		uint8_t  version() const { return ver_len >> 4; }
		uint8_t  hdr_len() const { return 4* (ver_len & 0xF); } 
		//uint8_t  services() const { return services_cong >> 2; }
		//uint8_t  congestion() const { return services_cong & 0xC0; }
		uint16_t total_len() const { return NET2HOST_PWORD(tot_len); }
		void total_len(uint16_t len) { HOST2NET_PWORD(len, tot_len); }
		uint16_t identification() const { return NET2HOST_PWORD(id); }
		void     identification(uint16_t v) { HOST2NET_PWORD(v,id); }

		// flags[0]  bits 7 6 5 4 3 2 1 0    Flags mask is ~(7 << 5)
		//                f f f
		void    set_flags(uint8_t mask) { flags[0] = (flags[0] & ~(7 << 5)) | (mask<<5); }
		uint8_t get_flags() const { return flags[0] >> 5; }
		uint16_t fragment_offset() const
		{
			uint16_t v = flags[0] & ~(7 << 5);
			v <<= 8;
			v += flags[1];
			return v;
		}
		uint16_t checksum() const { return  NET2HOST_PWORD(chksum); }
		void checksum(uint16_t v) { HOST2NET_PWORD(v,chksum); }
		uint8_t* data() const { return (uint8_t*) this+hdr_len(); }
	};	
private:
	uint16_t m_identifier;
	uint32_t m_ip;
public:
	IPProtocol() : m_identifier(1),m_ip(0) {}
	virtual int16_t handle(uint8_t *packet, size_type plen, size_type& processed);
	virtual int16_t send(uint8_t *packet, size_type maxlen, size_type& packet_len);

	bool is_for_me(Header *hdr);
	const uint8_t* ip_address() { return (uint8_t*)&m_ip; }
	void ip_address(uint8_t* new_ip) { memcpy(&m_ip, new_ip, sizeof(m_ip)); }
	bool is_ip_set() { return m_ip != 0; }
	bool is_my_ip(uint8_t* ip) { return is_ip_set() && memcmp(&m_ip,ip,sizeof(m_ip))==0; }
	int16_t send(uint8_t *packet, size_type packet_len, bool fill_header);
	
	static Header* get_header(const uint8_t *packet, size_type plen, size_type processed = sizeof(EthernetProtocol::Header));

	// sets len, id and checksum only
	static void set_header(Header *hdr, size_type packet_len, uint16_t id);
	// sets the full ip header
	static void set_header(Header *hdr,uint32_t dst_ip, uint32_t src_ip, size_type packet_len, uint16_t id);

	static uint8_t ip2str(char* buf, uint8_t len, const uint8_t* ip);
	static void print(const Header* packet);
	static uint16_t calc_checksum(const uint8_t* packet, uint16_t plen, uint32_t sum = 0);
};

typedef StaticSingletone<IPProtocol> IP;


class ICMPProtocol : public Protocol
{
public:
	enum { IP_TYPE = 1 };
private:
	enum ICMP_TYPE 
	{
		ICMP_REPLAY= 0,
		ICMP_DST_UNREACHABLE = 3,
		ICMP_REDIRECT = 5,
		ICMP_REQUEST = 8
	};
	
	struct Header
	{
		uint8_t type;
		uint8_t code;
		uint8_t hdr_checksum[2]; // checksum calculation starts from 'type' field
		uint8_t quench[4];
		uint16_t checksum() const { return NET2HOST_PWORD(hdr_checksum); }
		const char* type_str() const
		{
			static const char* str[] = { "RSP", "", "", "UNR", "", "RDR", "AH", "", "REQ"};
			return type < sizeof(str)/sizeof(str[0]) ? str[type] : "UNK";
		}
	};
	
	virtual int16_t handle(uint8_t *packet, size_type plen, size_type& processed);
	virtual int16_t send(uint8_t *packet, size_type maxlen, size_type& packet_len);

	static Header* get_header(const uint8_t *packet,uint16_t plen);
	static void print(const Header* packet);
	
};

class TCPProtocol : public Protocol
{
public:
	static const uint16_t max_window = MTU - sizeof(IPProtocol::Header) - sizeof(EthernetProtocol::Header);

	enum { IP_TYPE = 6 };
	enum Flags
	{
		FLAG_CWR = 128,// Congestion Window Reduced
		FLAG_ECE = 64, // ECN-Echo indicates: If the SYN flag is set (1), that the TCP peer is ECN capable. If the SYN flag is clear (0), that a packet with Congestion Experienced flag in IP header set is received during normal transmission (added to header by RFC 3168).
		FLAG_URG = 32, // indicates that the Urgent pointer field is significant
		FLAG_ACK = 16, // indicates that the Acknowledgment field is significant. All packets after the initial SYN packet sent by the client should have this flag set.
		FLAG_PSH = 8,  // Push function. Asks to push the buffered data to the receiving application.
		FLAG_RST = 4,  // Reset the connection
		FLAG_SYN = 2,  // Synchronize sequence numbers
		FLAG_FIN = 1   // No more data from sender
	};
	enum OptionKind
	{
		OPT_NOP = 1, // No -operation
		OPT_MSS = 2, // Maximum Segment Size, RFC 793
		OPT_WS  = 3, // Window Scale
	};
	struct Header
	{
		uint8_t sport[2];  // Source port
		uint8_t dport[2];  // Destination port
		uint8_t snum[4];   // Sequence number
		uint8_t anum[4];   // Ack num ( if ACK flag is set )
		
		uint8_t data_offset;     // 4 MSB specifies the size of header in 32bit words, 3 bits are reserved and the 
		                         // LSB is ECN-nonce concealment protection (added to header by RFC 3540)
		uint8_t flags;
		uint8_t window_size[2];
		uint8_t chksum[2];
		uint8_t urg_ptr[2];
		
		uint16_t src_port() const { return NET2HOST_PWORD(sport); }
		void src_port(uint16_t v) { HOST2NET_PWORD(v,sport); }
		uint16_t dst_port() const { return NET2HOST_PWORD(dport); }
		void dst_port(uint16_t v) { HOST2NET_PWORD(v,dport); }
		uint32_t seq_num() const  { return NET2HOST_PDWORD(snum); }
		void  seq_num(uint32_t v) { HOST2NET_PDWORD(v,snum); }
		uint32_t ack_num() const  { return NET2HOST_PDWORD(anum); }
		void  ack_num(uint32_t v) { HOST2NET_PDWORD(v,anum); }
		uint16_t hdr_len() const  { return (data_offset>>4) * 4; } 
		void hdr_len(uint16_t v)  
		{ 
			data_offset = ((v/4) <<4);
		}
		bool is_set(Flags flag) const { return flags & flag; }
		void set_flags(uint8_t mask) { flags = mask; }
		void add_flag(Flags f) { flags |= f; }

		void window(uint16_t v) { HOST2NET_PWORD(v,window_size); }
		uint16_t checksum() const { return  NET2HOST_PWORD(chksum); }
		void checksum(uint16_t v) { HOST2NET_PWORD(v,chksum); }
		uint8_t* data() const { return (uint8_t*)this + hdr_len(); }
		uint8_t* options() const { return hdr_len() > sizeof(*this) ? (uint8_t*)this+sizeof(*this) : 0; }
	};
	
	virtual int16_t handle(uint8_t *packet, size_type plen, size_type& processed);
	virtual int16_t send(uint8_t *packet, size_type maxlen, size_type& packet_len);
	virtual bool is_for_me(Header *hdr);
	void make_resp(Header* hdr, uint16_t request_datalen, uint16_t datalen);

	int16_t send(uint8_t *packet, size_type packet_len, bool fill_header);

	static void set_ports_from_request(Header* hdr);
	static void set_options_from_request(Header* hdr);
	static Header* get_header(const uint8_t *packet,uint16_t plen);
};

typedef StaticSingletone<TCPProtocol> TCP;

struct HTTPProtocol : public TCPProtocol
{
	enum { METHOD_GET, METHOD_POST };
	enum { MIN_BODY_LENGTH = 10 };
	enum ContextType { CONTEXT_TEXT, CONTEXT_JSON };
	typedef int16_t (*on_received_cb_type)(char* request, size_type max_len);

	virtual int16_t handle(uint8_t *packet, size_type plen, size_type& processed);
	virtual int16_t send(uint8_t *packet, size_type maxlen, size_type& packet_len);

	static int16_t fill_unauthorized(char* http_body, size_type maxlen);
	static int16_t fill_ok_header(char* http_body, size_type maxlen);
	static int16_t fill_context_type(char* http_body, size_type maxlen, ContextType ctype);
	on_received_cb_type on_get;
	on_received_cb_type on_post;
	static const char* methods[];
};

struct DHCPProtocol : public Protocol
{
	struct Header
	{
	};
	
	void setup()
	{
	}
	virtual int16_t handle(uint8_t *packet,size_type plen, size_type& processed)
	{
		return 0;
	}
};

#endif
